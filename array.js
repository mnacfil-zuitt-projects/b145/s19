
// // Section - Basic Array Structure

// let bootcamp = [
// 	'Learn HTML',
// 	'Use Css',
// 	'Understand JS',
// 	'Maintain MongoDB',
// 	'Create components using React'
// ];


// // console.log(bootcamp);
// console.log(bootcamp[0]);
// console.log(bootcamp[bootcamp.length -1]);
// console.log(bootcamp[6]); //undefined

// // length of array
// console.log(bootcamp.length)

// // if(bootcamp.length > 5); {
// // 	console.log(`this is how long array is, Do not exceed`)
// // }



// bootcamp.forEach(function(item, index){
// 	console.log(item, index)
// })

let students = [];
let sectionedStudents = [];

function addStudent(name) {
	students.push(name);
	console.log(`${name} has been added to the list of students`)
};
addStudent('melviN')
addStudent('maRie')
addStudent('Zai')
addStudent('cAirrie')
console.log(students);

function countStudents () {
	console.log(`This class has a total of ${students.length} enrolled students`)
}
countStudents()

function printStudents() {
	students = students.sort();
	students.forEach(student =>{
		console.log(student)
	})
}

printStudents()

function findStudents(keyword) {
	
	const filterStudents = students.filter(student => {
		return student.toLowerCase().includes(keyword.toLowerCase())
	});
	
	if(filterStudents === 1) {
		console.log('One student enrolled')
	} else {
		console.log(`${filterStudents.length} students enrolled`)
	}
}

findStudents('A')

function addSection(section) {
	sectionStudent = students.map(student => {
		return `${student} is part of section: ${section}`
	})
	console.log(sectionStudent)
}

addSection(3)